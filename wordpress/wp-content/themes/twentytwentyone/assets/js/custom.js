jQuery(document).ready( function($) {
	console.log('script loaded');
	
	$('.openbtn').on('click', function(){
		$('.nav-sidebar').toggleClass('show');
	});
	
	
	$('.staff').on('click','.approve,.decline', function() {
		let id = $(this).attr('data-id');
		let status = $(this).val();
		var self = this;
		
 		$.ajax({
			url: ajax_object.ajaxurl, // this is the object instantiated in wp_localize_script function
			type: 'POST',
			data:{ 
				action : 'update_clearance', // this is the function in your functions.php that will be triggered
				security : ajax_object.security,
				status : status,
				date_approved : dateToday(),
				post_id : id
			},
			success: function( data ){
				//Do something with the result from server
				console.log( data );
				if(data['success']) {
					displaySuccessMessage('Updated');
					$(self).closest('td').find('button').toggleClass('d-none');
					$(self).closest('tr').find('td.liability_status').text(status);
				}else {
					displayErrorMessage();
				}
			},
			error: function( error ){
				console.error(error);
				displayErrorMessage();
			}
		}); 
  
	});
	
	$( "#clearance-due" ).datepicker();
	
	$( "#clearance-form").on('submit', function(e) {
		e.preventDefault();
		let name = $('#clearance-name').val(),
			type = $('#clearance-type').val(),
			due = $('#clearance-due').val(),
			description = $('#clearance-description').val();
			
		console.log(formatDate(due));

		$.ajax({
			url: ajax_object.ajaxurl, // this is the object instantiated in wp_localize_script function
			type: 'POST',
			data:{ 
				action : 'create_clearance', // this is the function in your functions.php that will be triggered
				security : ajax_object.security,
				name : name,
				type : type,
				due : due,
				description : description
			},
			success: function( response ){
				//Do something with the result from server
				console.log( response );
				if(response['success']) {
					displaySuccessMessage();
					$('.close-modal').click();
					
					let searchParams = new URLSearchParams(window.location.search);
					
					let param = searchParams.has('type') ? searchParams.get('type') : '';
					
					if ( param == type ) {
						
						$('.staff-content-title').after(response['data']);
						// Increment count
						let count = $('.count').text();
						count = parseInt(count) + 1;
						$('.count').text(count);
						
						$('.not-found').remove();
					
					}
					
				} else {
					displayErrorMessage();
				}
			},
			error: function( error ){
				console.error(error);
				displayErrorMessage();
			}
		}); 
		
		
		
	});
	
	function displaySuccessMessage( status = 'Created' ) {
		let alertElem = `<div class="alert alert-success alert-dismissible d-flex align-items-center fade show">
									<i class="bi-check-circle-fill"></i>
									<strong class="mx-2">Success!</strong> Record has been ${status} Successfully.
									<button type="button" class="btn-close" data-bs-dismiss="alert"></button>
								</div>`;
					
		$('.alert-container').append(alertElem);
	}
	
	function displayErrorMessage() {
		let alertElem = `<div class="alert alert-danger alert-dismissible d-flex align-items-center fade show">
								<i class="bi-exclamation-octagon-fill"></i>
								<strong class="mx-2">Error!</strong> A problem has been occurred while saving your data.
								<button type="button" class="btn-close" data-bs-dismiss="alert"></button>
							</div>`;
		$('.alert-container').append(alertElem);
	}
	 
	function dateToday() {
		var d = new Date(),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;

		return year+month+day;
	}
	
	function formatDate( date ){
		
		var d = new Date( date ),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;

		return year+month+day;	
	}
	
	$('#staticBackdrop').on('hidden.bs.modal', function (event) {
		$('#clearance-name').val('');
		$('#clearance-description').val('');
		$('#clearance-due').val('');
	});
});