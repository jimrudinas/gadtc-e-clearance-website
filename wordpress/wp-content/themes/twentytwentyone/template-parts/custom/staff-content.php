<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 
$type = isset( $_GET['type'] ) ? $_GET['type'] : 'student';
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'clearance',
	'author'		=> get_current_user_id(),
	'meta_key'		=> 'clearance_type',
	'meta_value'	=> $type
);

// query
$clearances = new WP_Query( $args );
// debug($clearances);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class();?>>
	<!-- Success Alert -->
	<div class="entry-content staff has">
		<h2 class="mb-1"><?php echo ucfirst($type); ?></h2>
		<div class="staff-content-title mt-1">
			<h5>Total Clearance: <span class="count"><?php echo $clearances->post_count ?></span></h5>
			<!-- Button trigger modal -->
			<span><button 
					class="btn btn-secondary has-background" 
					data-bs-toggle="modal" 
					data-bs-target="#staticBackdrop">
					Add New
			</button></span>
		</div>
		
		<?php 
			if ( $clearances->posts ) :
				foreach($clearances->posts as $clearance) :
					set_query_var( 'post_id', $clearance->ID );
					get_template_part( 'template-parts/custom/single-clearance' );
				endforeach; 
			else: 
		?>
			<p class="not-found">No Clearance Found.</p>
		<?php endif; ?>
	</div>
	<!-- .entry-content -->
	<?php 
		get_template_part( 'template-parts/custom/staff-modal' );
	?>
	<footer class="entry-footer default-max-width">
		<?php twenty_twenty_one_entry_meta_footer(); ?>
	</footer><!-- .entry-footer -->
	<div class="alert-container fixed-top"></div>
</article><!-- #post-<?php the_ID(); ?> -->
