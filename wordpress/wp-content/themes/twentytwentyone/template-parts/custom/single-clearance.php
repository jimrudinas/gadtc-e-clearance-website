<?php	

/** @var int $post_id */
	
$clearance = get_post( $post_id );
$liabilities = gadtc_get_related_liabilities( $clearance->ID );
$count = 0;

?>
<div class="clearance-wrapper">
	<h4><?php echo $clearance->post_title; ?></h4>
	<?php if( $liabilities ) : ?>
	<div class="table-wrapper">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">First</th>
					<th scope="col">Last</th>
					<th scope="col">Date Created</th>
					<th scope="col">Date Approved</th>
					<th scope="col">Status</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($liabilities->posts as $liability) : 
				
				$liability_fields = get_fields( $liability->ID );
				$student = get_user_by( 'id', $liability_fields['liability_assigned_to'][0] );
				$approve_display = $liability_fields['liability_status'] == 'Pending' ? 'show' : 'd-none';
				$decline_display = $liability_fields['liability_status'] == 'Cleared' ? 'show' : 'd-none';
				// debug($student);
				$count++;
				?>
				<tr>
					<th scope="row"><?php echo $count; ?></th>
					<td><?php echo $student->first_name ?></td>
					<td><?php echo $student->last_name ?></td>
					<td><?php echo date('F j, Y', strtotime($liability->post_date)); ?></td>
					<td><?php echo isset($liability_fields['liability_date_approved']) ? $liability_fields['liability_date_approved'] : ''; ?></td>
					<td class="liability_status"><?php echo $liability_fields['liability_status']; ?></td>
					
					<td>
						<button 
							class="btn btn-success has-background approve <?php echo $approve_display; ?>" 
							value="Cleared"
							data-id="<?php echo $liability->ID;?>">Approve
						</button>
						<button 
							class="btn btn-danger has-background decline <?php echo $decline_display; ?>" 
							value="Pending"
							data-id="<?php echo $liability->ID;?>">Decline
						</button>
					</td>
				</tr>		
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php else : ?>
	<p>No Students Found.</p>
	<?php endif; ?>
</div>