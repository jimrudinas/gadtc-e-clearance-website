<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'liabilities',
	'meta_query'	=> array(
		'relation' 	=> 'AND',
			array(
				'key'		=> 'liability_assigned_to',
				'value'		=> get_current_user_id(),
				'compare'	=> '='
			),
			array(
				'key'		=> 'liability_status',
				'value'		=> 'Pending',
				'compare'	=> '='			
			)
	)
);


// query
$query = new WP_Query( $args );
$liability_count = $query->post_count > 1 ? 
						$query->post_count .' liabilities' : 
						$query->post_count . ' liability';
$count = 0;
$clearance_status = $query->post_count > 0 ? 'Not Cleared' : 'Cleared';
$status_class = $query->post_count > 0 ? 'text-danger' : 'text-success';
// debug( $query );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content student">
		<h2 class="mb-1">Clearance</h2>
		<div class="student-content-title mt-1">
			<h5>Status: <span class="<?php echo $status_class; ?>"><?php echo $clearance_status; ?></h5>
			<!-- Button trigger modal -->
			<span><a 
					href="<?php echo get_site_url() . '/history'?>"
					class="btn btn-secondary has-background" 
					>
					History
			</a></span>
		</div>
		<p>You have <?php echo $liability_count; ?></p>
		
		<div class="accordion" id="accordionPanelsStayOpen">
			<?php foreach( $query->posts as $post ): 
			$clearance_id = get_field( 'liability_clearance', $post->ID );
			$show = $count === 0 ? 'show' : '';
			$count++;
			$human_timing = human_time_diff( strtotime($post->post_date), strtotime( wp_date( 'Y-m-d H:i:s' ) ) ). ' ago';
			?>		
			
			<div class="accordion-item mb-1">
				<h2 class="accordion-header" id="panelsStayOpen-heading<?php echo $count; ?>">
					<button class="accordion-button has-background has-text-color" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapse<?php echo $count; ?>" aria-expanded="true" aria-controls="panelsStayOpen-collapse<?php echo $count; ?>">
					<?php echo get_the_title($clearance_id); ?>
					</button>
					<span class="human-timing"><?php echo $human_timing; ?></span>
				</h2>
				<div id="panelsStayOpen-collapse<?php echo $count; ?>" class="accordion-collapse collapse <?php echo $show; ?>" aria-labelledby="panelsStayOpen-heading<?php echo $count; ?>">
					<div class="accordion-body">
						<p class="mb-1">Description: <?php echo get_field( 'liability_description', $post->ID ); ?></p>
						<p class="mb-1">Due Date: <?php echo get_field( 'liability_due_date', $post->ID ); ?></p>
					</div>
				</div>
			</div>		

			<?php endforeach; ?>
		</div>
		
	</div>
	<!-- .entry-content -->

	<footer class="entry-footer default-max-width">
		<?php twenty_twenty_one_entry_meta_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->