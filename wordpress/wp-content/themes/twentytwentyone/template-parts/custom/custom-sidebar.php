<?php
/**
 * Displays the custom sidebar.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 $current_user = wp_get_current_user();
 
 $clearance_link = $current_user->roles[0] == 'subscriber' ? 
					get_site_url() . '/student' :
					get_site_url() . '/staff';
 
?>
<div class="nav-control">
	<button class="openbtn has-background" onclick="openNav()">☰</button>
</div>
<section class="nav-sidebar show">

	<div class="user-profile user-profile d-flex align-items-center p-2">
		<div class="icon">
			<i class="fas fa-user-circle"></i>
		</div>
		<div class="user-detail ms-2">
			<p class="user-name"><?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></p>
			<p class="user-status">
				<i class="fas fa-circle"></i>
				<span>Online</span>
			</p>
		</div>
	</div>
	
	<div class="search-box p-2">
		<input class="form-control" placeholder="Search..."/>
		<i class="fas fa-search"></i>
	</div>
	
	<div class="main-nav">
		<p class="mb-0">Main Navigation</p>
		
		<!--ul class="ps-0">
			<li><a><i class="fas fa-home me-1"></i>Home</a></li>
			<li><a href="#collapseExample" data-bs-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">
			<i class="fas fa-users me-1"></i>Student</a>
			</li>
			<div class="collapse student-action" id="collapseExample">
				<ul class="ps-4">
					<li><a><i class="far fa-id-badge me-2"></i>My Profile</a></li>
					<li><a><i class="fas fa-file-signature me-2"></i>My Enrolment</a></li>
					<li><a><i class="fas fa-medal me-2"></i>Grades</a></li>
					<li><a><i class="fas fa-tasks me-2"></i>Clearance</a></li>
					<li><a><i class="fas fa-certificate me-2"></i>Certificate of Registration</a></li>
				</ul>
			</div>
			<li><a><i class="fas fa-sign-out-alt me-1"></i>Logout</a></li>
		</ul-->
		
		<ul class="ps-0">
			<li><a href="<?php echo get_site_url(); ?>">Home</a></li>
			<?php if( $current_user->roles[0] == 'subscriber' ) : ?>
			<li><a href="#collapseExample" data-bs-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">Student</a>
			</li>
			<div class="collapse student-action" id="collapseExample">
				<ul class="ps-0">
					<li><a>My Profile</a></li>
					<li><a>My Enrolment</a></li>
					<li><a>Grades</a></li>
					<li><a href="<?php echo get_site_url() . '/student' ?>">Clearance</a></li>
					<li><a>Certificate of Registration</a></li>
				</ul>
			</div>
			<?php else : ?>
			<li><a href="#collapseStaff" data-bs-toggle="collapse" aria-expanded="false" aria-controls="collapseStaff">Clearance</a>
			</li>
			<div class="collapse student-action" id="collapseStaff">
				<ul class="ps-0">
					<li><a href="<?php echo get_site_url() . '/staff?type=student' ?>">Student</a></li>
					<li><a href="<?php echo get_site_url() . '/staff?type=graduation' ?>">Graduation</a></li>
					<li><a href="<?php echo get_site_url() . '/staff?type=credential' ?>">Credential</a></li>
				</ul>
			</div>
			<?php endif; ?>
			<li><a>Logout</a></li>
		</ul>
	</div>
</section>

