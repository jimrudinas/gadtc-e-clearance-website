<?php
/**
 * Displays the site header.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<header id="masthead"  role="banner">
	<div class="gadtc-site-branding d-flex justify-content-center align-items-center">
		<div class="site-logo"><?php the_custom_logo(); ?></div>
		<h1>Student Portal</h1>
	</div>
</header><!-- #masthead -->





