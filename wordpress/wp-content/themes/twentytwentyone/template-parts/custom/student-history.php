<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'liabilities',
	'meta_key'		=> 'liability_assigned_to',
	'meta_value'	=> get_current_user_id()
);


// query
$query = new WP_Query( $args );
$count = 0;
// debug( $query );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content history">
	
		<div class="student-content-title">
			<h5 class="mb-0">Archive</h5>
			<!-- Button trigger modal -->
			<span><a 
					href="<?php echo get_site_url() . '/student'; ?>"
					class="btn btn-secondary has-background" 
					>
					Go back
			</a></span>
		</div>
		
		<?php if ( $query->posts ) : ?>
			<table class="table table-bordered" >
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Liablity</th>
						<th scope="col">Description</th>
						<th scope="col">Date Created</th>
						<th scope="col">Date Approved</th>
						<th scope="col">Status</th>
					</tr>
				</thead>	
				<tbody>
					<?php foreach( $query->posts as $post ): 
					
					$liability_fields = get_fields( $post->ID );
					$status = $liability_fields['liability_status'] == 'Cleared' ? 'bg-success' : 'bg-danger';
					$count++;
					?>	
					<tr>
						<th scope="row"><?php echo $count; ?></th>
						<td><?php echo esc_html( $liability_fields['liability_clearance']->post_title ); ?></td>
						<td><?php echo $liability_fields['liability_description']; ?></td>
						<td><?php echo date('F j, Y', strtotime($post->post_date)); ?></td>
						<td><?php echo isset($liability_fields['liability_date_approved']) ? $liability_fields['liability_date_approved'] : ''; ?></td>
						<td class="<?php echo $status; ?>"><?php echo $liability_fields['liability_status']; ?></td>
					</tr>
	
				<?php endforeach; ?>
				</tbody>	
			</table>
		<?php else : ?>
			<p>No records found.</p>
		<?php endif; ?>
		
	</div>
	<!-- .entry-content -->

	<footer class="entry-footer default-max-width">
		<?php twenty_twenty_one_entry_meta_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->