<?php

?>
<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog mt-5">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="staticBackdropLabel">Add New Clearance</h5>
				<button type="button" class="has-background btn-close" data-bs-dismiss="modal" aria-label="Close"/>
			</div>
			<form id="clearance-form">
				<div class="modal-body">
						<div class="mb-3">
							<label for="clearance-name" class="col-form-label">Clearance Name:</label>
							<input class="form-control" id="clearance-name" required>
						</div>
						<div class="mb-3">
							<label for="clearance-description" class="col-form-label">Description:</label>
							<input class="form-control" id="clearance-description" required>
						</div>
						<div class="mb-3">
							<label for="clearance-type" class="col-form-label">Type:</label>
							<select id="clearance-type" class="form-select">
								  <option value="student" selected>Student</option>
								  <option value="graduation">Graduation</option>
								  <option value="credential">Credential</option>
							</select>
						</div>
						<div class="mb-3">
							<label for="clearance-due" class="col-form-label">Due Date:</label>
							<input class="form-control" id="clearance-due" required>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="has-background btn btn-secondary close-modal" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="has-background btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>