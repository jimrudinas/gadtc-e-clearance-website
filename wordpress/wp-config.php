<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'website1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3307' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'OiGhz+J>]{?#1x@9~-YaF}fe%qiKYO.piT)t X=MvNKCkCTGy|/T8m8iJ7SDB5L9' );
define( 'SECURE_AUTH_KEY',  'b{v+<fu}^LcMh6b!)U-=!.93^Ba~RU&nu~>.%*Nqp[JHxH;}!A}<TBFcgsHWwPXE' );
define( 'LOGGED_IN_KEY',    'D%5%Vi84},!T0YMy?T)3aX`~sAW@X|#6?= uT pIi&]Ujkp3SfitM`*7e(e;YsTO' );
define( 'NONCE_KEY',        'Ks;]q6$o`OZ7A~5dm0(o#KAbbSi#FOOVuVT-<D5j^#~>HSCf+4>hSa|dRv%Oj.`D' );
define( 'AUTH_SALT',        'E,-_Nisk1X?z{Rp)+zkM67z*d@J2mF(24h qj}gpBv=$|+OYmBP|0g,TqrP|-9Ff' );
define( 'SECURE_AUTH_SALT', 'VY=*iCs2w0vt:&9E+#5B#>$XS~2I.;%-V,+oJP0bD*{Bip=~Z]o6sZ&V1nC$.Y[@' );
define( 'LOGGED_IN_SALT',   '=v1%-sy)V@JBBub@F10j28SyyZh-/F(:0?uLd/~i{FKnkOsT@Z+y@)XF+d?2VBax' );
define( 'NONCE_SALT',       ' e(oXvE#%{>:x?`^Vp=`KSA: S./whd$S62Lgg>be[r)bi.W4uM`8tPzz0?}UI]b' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );
// define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/mysite');
// define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/mysite');

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
